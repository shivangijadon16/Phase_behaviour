import sys


def cosinesimilarity(list1,list2):
	a=[int(x) for x in list1]
	b=[int(x) for x in list2]
	result=(sum(ax*bx for ax,bx in zip(a,b))/((sum(ax**2 for ax in a))**0.5 * (sum(bx**2 for bx in b))**0.5))
	return result
	
	
totalinstructions=[]
cachereference=[]
icacheloadmisses=[]
dcacheloadmisses=[]
branchmisses=[]
unpacked=[]
for line in sys.stdin:
	line=line.strip()
	#print(line)
	unpacked.append(line.split(","))
	#one,two,three,four,five,six=line.split(",")
	
#print(unpacked)

for i in range(1,11):
	totalinstructions.append(unpacked[i][1])
	cachereference.append(unpacked[i][2])
	icacheloadmisses.append(unpacked[i][3])
	dcacheloadmisses.append(unpacked[i][4])
	branchmisses.append(unpacked[i][5])

unpacked1=[]
unpacked1.append(totalinstructions)
unpacked1.append(cachereference)
unpacked1.append(icacheloadmisses)
unpacked1.append(dcacheloadmisses)
unpacked1.append(branchmisses)
	
for i in range(1,5):
	for j in range(i+1,6):
		print(unpacked[0][0],": cosine similarity between",unpacked[0][i],"and",unpacked[0][j],"is:",cosinesimilarity(unpacked1[i-1],unpacked1[j-1]))
		


