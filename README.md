			WRITE UP (TASK-2)

Phase Behavior includes finding cache references, icache-misses, dcache-misses, total number of instructions and branch mispredictions using pref tool for any three or four linux 
applications. Gcc, grep, cat and cp are the linux applications chosen by us. We had 10 .c files for which we have calculated the required values using perf tool for gcc. For grep there 
was a single large text file in which different patterns were searched and cache misses etc were calculated. For cat and cp 10 different text files of different sizes were used. 
The second phase of our task was to find the cosine similarities for those calculated values using hadoop. Also for different threshold values of cosine similarities a bar graph is 
plotted against count which indicates number of phases exceeding the threshold value.
The final phase was to plot k means for the calculated data. We have chosen k=2 for a data set of 10 values and plotted it in jupyter notebook. This k-means plot shows the centroid of 
the clusters.

Learning:
1.	Understand the working of perf tool.
2.	Learnt commands of perf tool for different linux applications.
3.	Learnt to install and use Hadoop.
4.	Learnt to plot k-means and how k-means works in analyzing the data.

